// Constant category
export const CATEGORIES = [
    { id: 1, name: "Kinh Doanh" },
    { id: 2, name: "Thời Sự" },
    { id: 3, name: "Thể Thao" },
    { id: 4, name: "Giải Trí" },
    { id: 5, name: "Showbiz" },
    { id: 6, name: "Thời Sự" },
    { id: 7, name: "Thời Sự" },
    { id: 8, name: "Thời Sự" },
    { id: 9, name: "Thời Sự" },
    { id: 10, name: "Thời Sự" },
    { id: 11, name: "Thời Sự" },
    { id: 12, name: "Thời Sự" },
    { id: 13, name: "Thời Sự" },
    { id: 14, name: "Thời Sự" },
    { id: 15, name: "Thời Sự" },
    { id: 16, name: "Thời Sự" },
];

// Constant position
export const POSITIONS = [
    { id: 1, name: "Việt Nam" },
    { id: 2, name: "Châu Á" },
    { id: 3, name: "Châu Âu" },
    { id: 4, name: "Châu Mỹ" },
];

//Base url API
export const BASE_API = "http://localhost:3000/blogs/";


